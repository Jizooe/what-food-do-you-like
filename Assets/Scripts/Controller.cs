using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Controller : MonoBehaviour
{
    public TextMeshProUGUI Result;
    public Image AIChoice;

    public string[] Choices;
    public Sprite Rock, Paper, Scissors;

    private HealthBarController _healthBarController;

    private void Start()
    {
      _healthBarController = GetComponent<HealthBarController>();
    }

    public void Play(string myChoice)
    {
        string randomChoice = Choices[Random.Range(0, Choices.Length)];

        switch(randomChoice)
        {
            case "Rock":
                switch(myChoice)
                {
                    case "Rock":
                        Result.text = "I love it."; 
                        _healthBarController.TakeDamage(10,false );
                        break;

                    case "Paper":
                        Result.text = "Cry";
                        _healthBarController.TakeDamage(10,true);
                        break;

                    case "Scissors":
                        Result.text = "Hungry";
                        _healthBarController.TakeDamage(10,true);
                        break;
                    
                }

                AIChoice.sprite = Rock;
                break;

            case "Paper":
                switch (myChoice)
                {
                    case "Rock":
                        Result.text = "I'm so hangry!";
                        _healthBarController.TakeDamage(10,true);
                        break;

                    case "Paper":
                        Result.text = "Kiss Kiss";
                        _healthBarController.TakeDamage(10,false);
                        break;

                    case "Scissors":
                        Result.text = "So sad";
                        _healthBarController.TakeDamage(10,true);
                        break;

                }

                AIChoice.sprite = Paper;
                break;

            case "Scissors":
                switch (myChoice)
                {
                    case "Rock":
                        Result.text = "Cry";
                        _healthBarController.TakeDamage(10,true);
                        break;

                    case "Paper":
                        Result.text = "I'm so hangry!";
                        _healthBarController.TakeDamage(10,true);
                        break;

                    case "Scissors":
                        Result.text = "Mwah Mwah";
                        _healthBarController.TakeDamage(10,false);
                        break;

                }

                AIChoice.sprite = Scissors;
                break;

        }
    }
    
}