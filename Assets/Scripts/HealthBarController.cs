using UnityEngine;
using TMPro;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthBarController : MonoBehaviour
{
    //หลอดเลือด
    public Slider playerSlider;
    public Slider aiSlider;
    
    //โชว์หน้าwin/lose
    public GameObject winCanvas;
    public GameObject loseCanvas;
    public GameObject HealCanvas;

    [Header("Player")]
    public int playerHP;
    public int maxPlayerHP = 10;

    [Header("AI")] 
    public int aiHP;
    public int maxAiHP = 10;
    
    //HealPlayer
    private int healPlayerCount = 0;
    private const int maxHealPlayerCount = 3;
    public TextMeshProUGUI healCountText;
    
    void Start()
    {
        // ปิดการมองเห็นของ Canvas ทั้งสอง
        winCanvas.gameObject.SetActive(false);
        loseCanvas.gameObject.SetActive(false);
        
        //player
        playerHP = maxPlayerHP;
        playerSlider.maxValue = maxPlayerHP;
        playerSlider.value = playerHP;
        
        //ai
        aiHP = maxAiHP;
        aiSlider.maxValue = maxAiHP;
        aiSlider.value = aiHP;
        
        // อัปเดตค่าของ Text UI element เมื่อเริ่มต้น
        UpdateHealCountText();
    }

    public void TakeDamage(int amount, bool isPlayer)
    {
        if (isPlayer)
        {
            playerHP -= amount;
            playerSlider.fillRect.GetComponent<Image>().color = Color.Lerp(Color.red, Color.green, (float)playerHP / maxPlayerHP); // กำหนดสีของแถบเต็มของ Slider โดยอ้างถึงส่วน fillRect และกำหนดสีโดยใช้ Lerp ระหว่างสีแดงและสีเขียว
            playerSlider.value = playerHP;
            if (playerHP <= 0)
            {
                Debug.Log("LOSE AI");
                loseCanvas.gameObject.SetActive(true); // เมื่อ playerHP <= 0 เปิดการมองเห็น Canvas สำหรับหน้า Lose
                HealCanvas.gameObject.SetActive(false); // ปิดปุ่มheal
            }
        }
        else
        {
            aiHP -= amount;
            aiSlider.fillRect.GetComponent<Image>().color = Color.Lerp(Color.red, Color.green, (float)aiHP / maxAiHP);
            aiSlider.value = aiHP;
            if (aiHP <= 0)
            {
                Debug.Log("Player WIN");
                winCanvas.gameObject.SetActive(true); // เมื่อ aiHP <= 0 เปิดการมองเห็น Canvas สำหรับหน้า Win
                HealCanvas.gameObject.SetActive(false); // ปิดปุ่มheal
            }
        }
    }
    
    //เพิ่มเลือดPlayer
    public void HealPlayer(int amount)
    {
        // ตรวจสอบว่ายังสามารถเรียกใช้งานเมธอด HealPlayer ได้อีกหรือไม่
        if (healPlayerCount < maxHealPlayerCount)
        {
            playerHP += amount;
            playerSlider.fillRect.GetComponent<Image>().color = Color.Lerp(Color.green, Color.green, (float)playerHP / maxPlayerHP);
            playerHP = Mathf.Clamp(playerHP, 0, maxPlayerHP);
            playerSlider.value = playerHP;
            healPlayerCount++; // เพิ่มจำนวนครั้งที่เรียกใช้งานเมธอด HealPlayer
            
            // อัปเดตค่าของ Text UI element
            UpdateHealCountText();

            // ตรวจสอบว่าเมื่อเรียกใช้งานเมธอด HealPlayer ครบจำนวนครั้งแล้วหรือไม่
            if (healPlayerCount >= maxHealPlayerCount)
            {
                Debug.Log("Maximum usage of HealPlayer reached.");
            }
        }
        else
        {
            Debug.Log("HealPlayer method can only be used " + maxHealPlayerCount + " times.");
        }
    }
    
    private void UpdateHealCountText()
    {
        if (healCountText != null)
        {
            healCountText.text = "" + (maxHealPlayerCount - healPlayerCount);
        }
    }
}

