using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMusic : MonoBehaviour
{
    private static BGMusic _bgMusic;

    private void Awake()
    {
        if (_bgMusic == null)
        {
            _bgMusic = this;
            DontDestroyOnLoad(_bgMusic);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
